\select@language {spanish}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {spanish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n y Objetivos}{2}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{2}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Objetivos generales y espec\IeC {\'\i }ficos}{4}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Objetivo General}{4}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{4}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Alcances}{5}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Descripci\IeC {\'o}n de cap\IeC {\'\i }tulos}{6}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Antecedentes}{8}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Introducci\IeC {\'o}n}{8}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Hitos m\IeC {\'a}s importantes de la Neurociencia}{9}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Interacci\IeC {\'o}n entre Neurociencia e Ingenier\IeC {\'\i }a El\IeC {\'e}ctrica}{11}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}La Neurona como un circuito el\IeC {\'e}ctrico}{11}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}El Ax\IeC {\'o}n como linea de transmisi\IeC {\'o}n}{12}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}La neurona como codificador analogo digital}{13}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Los circuitos neuronales como controladores \IeC {\'o}ptimos}{14}{subsection.2.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Panorama mundial}{15}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Programas gubernamentales e intergubernamentales}{16}{subsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Human Connectome Project (HCP) - Estados Unidos}{16}{section*.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{BRAIN Initiative - Estados Unidos}{17}{section*.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Blue Brain Project (BBP) - Europa}{18}{section*.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Human Brain Project (HBP) - Europa}{19}{section*.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Programas acad\IeC {\'e}micos}{20}{subsection.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Massachusetts Institute of Technology (MIT) - Estados Unidos}{20}{section*.8}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{Stanford University - Estados Unidos}{21}{section*.9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\IeC {\'E}cole des Neurosciences de Paris (ENP) - Francia}{22}{section*.10}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Panorama Nacional}{24}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Ingenier\IeC {\'\i }a Civil Biom\IeC {\'e}dica}{24}{subsection.2.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}Programas de Doctorado en Neurociencias}{25}{subsection.2.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Descripci\IeC {\'o}n del problema}{26}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Conclusiones}{26}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Contenidos}{28}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Motivaci\IeC {\'o}n}{28}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Dise\IeC {\~n}o del curso}{29}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Introducci\IeC {\'o}n}{29}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Primera Unidad: Introducci\IeC {\'o}n al curso}{30}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Segunda Unidad: Bases el\IeC {\'e}ctricas de las neuronas}{30}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Apuntes para la Unidad 2}{33}{appendix.Alph1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Ecuaci\IeC {\'o}n de Nernst}{33}{section.Alph1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}Ecuaci\IeC {\'o}n de Goldman-Huxley-Katz}{34}{section.Alph1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}Ecuaci\IeC {\'o}n de Hodking y Huxley}{35}{section.Alph1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.4}Ecuaciones de canales}{39}{section.Alph1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.4.1}Ecuaciones de Canal de Sodio.}{40}{subsection.Alph1.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.4.2}Ecuaciones de Canal de Potasio}{41}{subsection.Alph1.4.2}
