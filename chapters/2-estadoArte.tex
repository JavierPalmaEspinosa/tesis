\chapter{Antecedentes}
\label{cap:antecedentes}
\section{Introducción}
Este capítulo presenta la interacción existente entre Neurociencia e Ingeniería Eléctrica.  Esta interacción no es nueva y se ha venido desarrollando a lo largo de la historia de la humanidad.  Por ello, la primera parte de este capítulo menciona ciertos avances e hitos logrados en el campo de la neurociencia, relacionados principalmente con fenómenos eléctricos.  Posteriormente se mostrará de forma más directa esta interacción.  Para ello, se tomarán cuatro áreas de la ingeniería eléctrica: Redes eléctricas, Lineas de Transmisión, Teoría de la Información y Teoría de control, y se mostrará cómo estas herramientas son aplicables directamente a la Neurociencia.
A continuación, se enuncian los proyectos internacionales e intergubernamentales que han logrado utilizar esta interdisciplinariedad para generar avances en el campo de la neurociencia.
Finalmente, se muestran cómo las universidades entienden esta interacción.  Para ello se muestran los programas de pre y postgrado a nivel internacional y también a nivel nacional.
\section{Hitos más importantes de la Neurociencia}
La neurociencia no es una disciplina nueva, ya que el primer registro sobre el sistema nervioso data del antiguo egipto\cite{dawson1931edwin}.   A continuación se muestra un listado no extensivo de los grandes hitos de esta disciplina a lo largo de la historia de la humanidad\footnote{Información extraída de \url{https://faculty.washington.edu/chudler/hist.html}}, enfocado principalmente en aquellos relacionados con eventos eléctricos o que han utilizado técnicas y herramientas provenientes de la Ingeniería Eléctrica. 
\begin{itemize}
\item{1700 a.C. Primer registro escrito sobre el sistema nervioso, encontrado en un papiro en egipto.}
\item{460-379 d.C. - Hipocrates discute la epilepsia como un disturbio del cerebro.}
\item{1587 - Guilio Cesare Aranzi describe los ventrículos y el hipocampo. También demostró que la retina posee una imagen invertida}
\item{1760 - Arne-Charles Lorry demuestra que el daño al cerebelo afecta la coordinación motora.}
\item{1791 - Luigi Galvani publica su trabajo sobre estimulación eléctrica en nervios de sapos.}
\item{1809 - Luigi Rolando usa corrientes galvánicas para estimular la corteza cerebral.}
\item{1818 - Mary Shelley publica Frankenstein}
\item{1849 - Hermann von Helmholtz mide la velocidad de la transmisión del impulso nervioso en un sapo.}
\item{1875 - Richard Caton es el primero en grabar la actividad eléctrica del cerebro.}
\item{1929 - Hans Berger publica sus descubrimientos sobre el primer electroencefalograma (EEG) humano.}
\item{1949 - Kenneth Cole desarrolla la técnica de voltage clamp}
\item{1963 - John Carew Eccles, Alan Lloyd Hodgkin y Andrew Fielding Huxley comparten el Premio Nobel por su trabajo en el mecanismo de la membrana de las neuronas.}
\item{1976 - Erwin Neher y Bert Sakmann desarrollan la técnica de patch-clamp}
\item{1985 - Eric L. Schwartz acuña el término Neurociencia Computacional}
\item{1990 - Humberto Maturana lanza el libro "Neurociencia y Cognición: Biología de lo Psíquico".}
\item{2005 - Se funda la Sociedad Chilena de Neurociencia}
\item{2005 - Se lanza el Blue Brain Project}
\item{2013 - El presidente de Estados Unidos, Barack Obama, anuncia la iniciativa BRAIN  (Brain Research through Advancing Innovative Neurotechnologies).}
\item{2014 - Un parapléjico, usando un exoesqueleto controlado por su cerebro, da el puntapié para la Copa del Mundo de fútbol, en Brasil}
\item{2015 - Cientificos de la Universidad de St. Louis, Estados Unidos, son capaces de controlar el cerebro de un ratón mediante tecnología wi-fi\cite{jeong2015wireless}}
\end{itemize}
\section{Interacción entre Neurociencia e Ingeniería Eléctrica}
Debido a la interdisciplinariedad actual de la neurociencia, la ingeniería eléctrica se ha ido involucrando más en los experimentos y métodos desarrollados en neurociencia\cite{Lewis1968,Lewis1968-IEEE}, ya sea como disciplina de ayuda, proveyendo de técnicas y metodologías para la resolución de problemas de adquisición y análisis de datos o planteando nuevas hipótesis y generando nuevas tecnologías a partir de los descubrimientos realizados por el área biológica de la neurociencia, tal como se muestra en el capítulo 1 de \cite{eliasmith2004neural}.

La interacción entre neurociencia e ingeniería eléctrica no es nueva.  Como se mostró anteriormente, con los experimentos de Galvani  y en mayor medida con los experimentos de Hodgkin y Huxley\cite{HODGKIN1952} la ingeniería eléctrica ha tenido un rol muy importante en el desarrollo de la neurociencia en los últimos 50 años.
\subsection{La Neurona como un circuito eléctrico}
En 1952, Hodgking y Huxley realizaron un experimento, en el cual sugieren que el comportamiento eléctrico de la membrana celular de una neurona de calamar gigante (\textit{loligo fobressi}) puede ser representada como una red eléctrica como la que se muestra en la figura \ref{fig:hh-paper.png}. 
En particular, si se aplican las ecuaciones de Kirchoff para las corrientes que fluyen desde el medio extracelular al intracelular, es posible plantear a la neurona completa a través de la siguiente ecuación

$$I_{ext} = C_m\frac{dV_m}{dt} + g_K(V_m - V_K) + g_{Na}(V_m - V_{Na}) + g_l(V_m - V_l)$$

Donde:
$g_K$ es la conductancia de los canales de potasio.\\
$g_{Na}$ es la conductancia de los canales de sodio.\\
$g_l$ es una conductancia genérica de "leak".\\
$V_m$ es el voltaje de membrana de la neurona en estudio.\\


\begin{figure}[!ht]
  \centering
    \includegraphics[scale = 0.5]{./images/membranaHH.png}
      \caption{Modelo eléctrico de la neurona, mostrado por Hodgkin y Huxley en su publicación de 1952\cite{HODGKIN1952}.}
      \label{fig:hh-paper.png}
\end{figure}

El formalismo matemático planteado por Hodgkin y Huxley, permitió un estudio mucho más profundo de los canales iónicos (cita) y sentó las bases para futuros análisis computacionales de las neuronas\cite{markram2006blue}
\subsection{El Axón como linea de transmisión}
En la figura  \ref{fig:neurona.png}, es posible observar el cuerpo o soma de la neurona, su axón y dendritas.  El axón de una neurona, presenta diversos canales iónicos, que pueden ser modelados como lo mostró hodgkin y huxley.  Además, el axón propaga el impulso eléctrico o potencial de acción, desde el soma hasta los terminales presinápticos (cita).
Es esta característica la que llevó a diferentes científicos a considerar al axón como un cable eléctrico.  En particular, W. Rall publicó una serie de papers que derivan un caso particular de la ecuación del telegrafista, basado en un análisis eléctrico del axón.  Adicionalmente, fue Rall el que mostró, utilizando la teoría del cable aplicada al axón, y las ecuaciones de hodgkin y huxley, que las dendritas podrían ser descritas como un cilindro perfecto, dividido en segmentos definidos o compartimentos\cite{rall2011core}.
Es gracias a esta simplificación en la geometría que los actuales softwares de simulación neuronal basan su funcionamiento, como por ejemplo NEURON\cite{hines1997neuron}.

\begin{figure}[!ht]
  \centering
    \includegraphics[scale = 0.25]{./images/neurona.png}
      \caption{Dibujo de una neurona, señalando sus partes más características.  Imagen con licencia CC BY-SA.}
      \label{fig:neurona.png}
\end{figure}

\subsection{La neurona como codificador analogo digital}
El sistema nervioso no puede ser pensado como un sistema autónomo y desconectado del resto del cuerpo humano.  Una de sus principales funciones consiste en capturar información del medio ambiente y traducirlo en señales capaces de ser interpretadas por el cerebro(cita).
Debido a que la forma de "comunicación" de una neurona es a través de su potencial de acción, es factible pensar que la información que quiere transmitir la neurona está codificado, de alguna forma, en dicho potencial o potenciales de acción. (cita)
Las interrogantes que surgen son entonces ¿cómo representa el sistema nervioso el ambiente que nos rodea? ¿Cómo es capaz de traducir los estímulos como luz, colores, olores, sabores, en información útil para nosotros?
La funcion de codificación de una neurona o de una población de neuronas es el campo de investigación de la  codificacion neuronal.  En ella, se utilizan técnicas de teoría de la información y del código para poder deducir cómo ciertas poblaciones de neuronas convierten los estímulos del medio ambiente en información procesable por el cerebro. (cita)

En 1988, van Stevenick y W. Bialek realizaron un experimento en el que sentaron las bases de lo que podría considerarse la era moderna del análisis de la teoría de la información en neurociencia\cite{dimitrov2011information}. En su estudio, ellos pudieron cuantificar la información contenida en diferentes propiedades de los potenciales de acción\cite{van1988real}.  Quizá una de las conclusiones mas importantes de su estudio fue la cuantización de la información transmitida en un potencial de acción: 0.36 bits.  Adicionalmente, plantean que dos potenciales de acción seguidos entregan más información que cada uno de ellos por separado, lo que da indicios de la existencia de información condicional.
Con lo anterior, se hace claro que las aplicaciones de la teoría de Shannon\cite{shannon1948} son de gran utilidad en neurociencia.
\subsection{Los circuitos neuronales como controladores óptimos}
Quizá una de las habilidades que damos por sentadas en el sistema nervioso es el movimiento.  Nos resulta natural caminar, tomar un objeto o bailar.  Sin embargo, los cálculos que debe realizar el cerebro, así como la coordinación oculo-motora no es una tarea fácil de realizar.
Si se considera que el sistema motor funciona como un lazo de control, pueden analizarse varios casos, siendo los más importantes el lazo abierto y el control de movimientos en lazo cerrado\cite{scott2004optimal,todorov2004optimality}.
En el caso del lazo abierto, es relativamente fácil establecer modelos que sean optimos, ya sea en maximizar una función o tarea asignada, minimzar energía, etc\cite{todorov2004optimality}.
Sin embargo, analizar el lazo cerrado es muchísimo más complejo. ¿cómo es posible coordinar una tarea tan compleja como atrapar un objeto en movimiento?\cite{scott2004optimal,todorov2004optimality}.

\begin{figure}[!ht]
  \centering
    \includegraphics{./images/controlNature.png}
      \caption{Dibujo esquemático de control óptimo en lazo cerrado. Imagen extraída de \cite{scott2004optimal}, modificada de\cite{todorov2004optimality}.}
      \label{fig:controlNature.png}
\end{figure}

Todorov y Jordan propusieron la teoría de que la coordinación motora emplea un control óptimo mediante lazo cerrado\cite{todorov2002optimal}, empleando el sistema visual como lazo de feedback.
Se observa entonces que las teorias de control son altamente aplicables en neurociencia\cite{scott2004optimal}.

\section{Panorama mundial}
La integración del campo de la neurociencia con la ingeniería no es un tema novedoso.  Desde hace años que existen programas interdisciplinarios a nivel mundial y desde hace una década aproximadamente, grandes proyectos internacionales han sido realizados para resolver problemas de investigación en neurociencia.  Lo importante de dichos proyectos es que son altamente interdisciplinarios, en donde biólogos, físicos, ingenieros y otros profesionales han colaborado directamente en la ejecución e implementacion de tales tareas.
A continuación se presentará una perspectiva global, tanto de los proyectos internacionales en investigacion en neurociencia, como también los programas academicos existentes en el mundo, enfocados principalmente en Europa y Estados Unidos, debido a que culturalmente, son los lugares a los cuales se aspira a llegar en Chile.
\subsection{Programas gubernamentales e intergubernamentales}
\subsubsection{Human Connectome Project (HCP) - Estados Unidos}
El Proyecto del Conectoma Humano es un esfuerzo realizado por el gobierno estadounidense (National Institute of Health (NIH), Instituto Nacional de Salud), para realizar un mapeo de las conexiones y vías que estan relacionadas con las funciones humanas.  El propósito mayor de este proyecto es adquirir y compartir datos acerca de las conexiones estructurales y funcionales del cerebro humano\footnote{\url{https://www.nih.gov/news-events/news-releases/nih-launches-human-connectome-project-unravel-brains-connections}}.  
Este proyecto fue lanzado el año 2009 y cuenta con dos grupos de trabajo. El primero es liderado por la Universidad de Washington en Saint Louis, junto a la Univerisdad de Minnesota y la Universidad de Oxford\footnote{\url{https://www.humanconnectome.org/about/project/}}.  El segundo grupo es liderado por la Universidad de Harvard, en conjunto con el Hospital General de Massachusetts y la Universidad de California - Los Angeles (UCLA).
El primer grupo utilizará resonancias magnéticas con equipos de 3 y 7 T, y proveerá datos de acceso libre, sobre la conectividad del cerebro, sus relaciones con el comportamiento y las contribuciones de los factores genéticos y ambientales en las diferencias individuales de los circuitos del cerebro.
Por otro lado, el segundo grupo utilizará resonancias magnéticas de 3T. Sus metas son el diseño de protocolos de adquisición de datos más eficientes, desarrollo de nuevos algoritmos para el análisis de la estructura de las fibras y de la conectividad inter-regional.  Adicionalmente pretenden desarrollar nuevos medios para la interacción y navegación gráfica de la conectividad del cerebro\footnote{\url{http://www.neuroscienceblueprint.nih.gov/connectome/}}.
\subsubsection{BRAIN Initiative - Estados Unidos}
La Iniciativa BRAIN (Brain Research through Advancing Innovative Neurotechnologies, Investigación Cerebral a través del Avance en Neurotecnologías Innovativas) pretende ayudar a los investigadores a encontrar nuevas formas para tratar, curar y prevenir enfermedades neuronales, tales como el Alzheimer o epilepsia\footnote{\url{https://www.whitehouse.gov/the-press-office/2013/04/02/fact-sheet-brain-initiative}}.
El proyecto, anunciado el año 2013, durará 12 años y tendrá un financiamiento de 4500 millones de dólares. Contará con aportes tanto de las universidades estadounidenses como de empresas privadas.
La iniciativa BRAIN buscará realizar un mapeo de los circuitos del cerebro, medir las fluctuaciones en los patrones de la actividad química y eléctrica dentro de dichos circuitos y finalmente, entender cómo esas interacciones crean nuestras capacidades únicas, tanto cognitivas como de comportamiento\footnote{\url{https://www.nih.gov/news-events/news-releases/nih-embraces-bold-12-year-scientific-vision-brain-initiative}}
Para lograr tales propósitos, BRAIN trabajará con una serie de metas a corto y largo plazo. Sin embargo, para lograr dichas metas, BRAIN se apoyará en 7 principios básicos:
\begin{itemize}
\item{Realizar en paralelo, modelos de estudios en humanos y no humanos}
\item{\bf{Cruzar barreras en colaboraciones interdisciplinarias}}
\item{Integrar escalas espaciales y temporales}
\item{Establecer plataformas para preservar y compartir datos}
\item{Validar y diseminar la tecnología}
\item{Considerar las implicaciones éticas de la investigación neurocientífica}
\item{Crear mecanismos para asegurar la responsabilidad de los científicos con el NIH y los contribuyentes}
\end{itemize}
\subsubsection{Blue Brain Project (BBP) - Europa}
El Blue Brain Project es un proyecto multinacional, iniciado el año 2005, que tiene como objetivo realizar una simulación a nivel molecular de todo el cerebro de un ratón. Su sede central está en Ginebra, Suiza,   en particular en la École Polytechnique Fédérale de Lausanne.  Además de su sede en Suiza, el BPP cuenta con colaboraciones de la empresa IBM (Estados Unidos), quién vendió su supercomputadora Blue Gene a un precio más barato que lo usual, para que ésta fuese probada en ambientes "reales".  España también forma parte del BBP, con su proyecto denominado Cajal Blue Brain. La Universidad de Yale ha colaborado de forma indirecta, a través de Michel Hines y sus comentarios sobre la implementación de software para simulación.  También colaboran de forma esporádica la Universidad de Nevada (Estados Unidos), el St. Elizabeth Medical Center(Estados Unidos), la Universidad de Londres (Inglaterra) y la Universidad Hebrea de Jerusalén (Israel)\footnote{\url{http://bluebrain.epfl.ch/cms/lang/en/pid/56897}}.
Diversas simulaciones señalan que la reconstrucción llevada a cabo en el BBP pueden reproducir comportamientos reportados por otros laboratorios a nivel mundial, según se señala en la página del proyecto\footnote{\url{http://bluebrain.epfl.ch/page-125344-en.html}}.  Esto significaría una nueva herramienta para realizar experimentos y obtener resultados que de otra forma no sería posible, debido a lo complejo de las interacciones biológicas que existen en un cerebro real\cite{markram2006blue} \\
\subsubsection{Human Brain Project (HBP) - Europa} 
Siguiendo los pasos del BPP, el año 2013 se lanza el Human Brain Project.  Un proyecto muchísimo más ambicioso que su predecesor.  
El HBP pretende utilizar tecnología de la información y las comunicaciones para generar infraestructura que permita desarrollar investigación de punta, ayudando a la comunidad científica e industrial avanzar en los campos de neurociencia, computación y medicina relacionada con el cerebro\cite{HBProject2015}.
Al igual que el BBP, el HBP se encuentra localizado en Ginebra, Suiza, contando con más de 100 colaboraciones a lo largo de toda Europa.
Este proyecto pretende desarrollar tecnología en Neuroinformática, Simulación cerebral, High Performance Computing, Informática médica, computación neuromorfica, neurorobótica\cite{HBProject2015}.
A pesar del apoyo a gran escala de la Union Europea y de la gran cantidad de colaboraciones, el HBP ha tenido numerosas críticas.  En el año 2014, una carta firmada por más de 800 científicos de toda Europa han señalado que este proyecto carece de ciertas directrices de fondo, además de haber dejado de lado a una veintena de laboratorios, cada uno de ellos con claros objetivos teóricos más que computacionales, criticando de esta forma la interdisciplinareidad y los acuerdos de colaboración que fundan las intenciones del proyecto\footnote{\url{http://www.neurofuture.eu/}}. 
En particular, Frégnac y Laurent señalan que ''el construir gigantescas bases de dato para alimentar simulaciones sin los loops de correción entre las hipótesis y las pruebas experimentales son, en forma optimista, una pérdida de tiempo y de dinero.  La meta del HPB se parece más a una costosa expansión del BBP, sin ninguna evidencia que pueda producir un entendimiento más profundo del cerebro humano"\cite{fregnac2014neuroscience}.

\subsection{Programas académicos}
En esta sección se mostrarán tanto los programas de pre y postgrado que se imparten en diversas universidades, tanto en Estados Unidos como en Europa. Si bien existen numerosos programas de neurociencia en dichas regiones, este listado no pretende ser extenso, sino más bien mostrar la forma en que se ha abordado la neurociencia en paises desarrollados.
\subsubsection{Massachusetts Institute of Technology (MIT) - Estados Unidos}
El Instituto Tecnológico de Massachusetts (MIT) es uno de los referentes a nivel mundial en ciencia, tecnología e ingeniería, pues en sus aulas han pasado 85 premios nobel\footnote{\url{http://web.mit.edu/ir/pop/awards/nobel.html}}, además de haber sido la mejor universidad del mundo desde el año 2012\footnote{\url{http://www.topuniversities.com/universities/massachusetts-institute-technology-mit\#wur}}. 
Si bien su especialización más fuerte es en ingeniería, el MIT imparte otros programas, neurociencia entre ellos.
En particular, el MIT posee el departamento del cerebro y ciencias cognitivas (Brain and Cognitive Sciences), el cual ofrece formación de pre y postgrado en Neurociencia\footnote{\url{https://bcs.mit.edu}}.
En el pregrado, los estudiantes salen con un "minor", equivalente a una mención en ingeniería. Este minor cuenta con varias subdisciplinas con una gran variedad de electivos interdisciplinarios que, junto a los intereses del estudiante, permite tanto seguir en investigación como dedicarse a otras funciones laborales.

\begin{figure}[!ht]
  \centering
    \includegraphics[scale = 0.5]{./images/MIT_Tier.png}
      \caption{Programa del MIT, mostrando la estructura del minor en neurociencia.  Extraido desde \url{https://bcs.mit.edu}}
      \label{fig:MIT_Tier.png}
\end{figure}

Dentro de las subdisciplinas elegibles estan: Neurociencia de Sistemas, Neurociencia Celular y Molecular, Ciencias Cognitivas (con especializaciones en lenguaje, desarrollo cognitivo o neurociencia cognitiva) y Computación\footnote{\url{https://bcs.mit.edu/sites/default/files/files/BCS_recommended\%20classes_combined\%20updated\%20160209.pdf}}.
El programa de postgrado en neurociencia, al igual que la mayoría de los programas de doctorado en neurociencia en Estados Unidos, exige una rotación entre 3 laboratorios durante el primer año, para posteriormente enfocarse directamente en la tesis de doctorado.
Las áreas de investigación ofrecidas en el MIT son: Neurociencia Celular y Molecular, Neurociencia de Sistemas, Neurociencia Computacional, Ciencias Cognitivas.  Sin embargo, se ofrece la opción de que el estudiante pueda elegir algo que no esté dentro de las áreas mencionadas anteriormente.
Cabe destacar que el MIT es miembro de la iniciativa CDIO.  Esta iniciativa plantea que los ingenieros egresados deben ser capaces de concebir, diseñar, implementar y operar sistemas de ingeniería.  Se hablará más de la iniciativa CDIO en el capitulo \ref{cap:metodos}.
\subsubsection{Stanford University - Estados Unidos}
La Universidad de Stanford es una de las universidades privadas más prestigiosas del mundo. Se encuentra emplazada en la meca de la revolución tecnológica de mediados de los años 80: Sillicon Valley.  
Stanford ha sido la cuna de variadas empresas relacionadas con la informática y las telecomunicaciones.  Google, Sun Microsystems, Hewlett-Packard, Cisco Systems y otras han sido desarrolladas por egresados de esta universidad.
El programa de doctorado en neurociencia en Stanford empezó el año 1962. Según la filosofía de la Universidad, la idea al fundar este programa fue la de \textit{ofrecer un programa coherente y único en neurociencia, contrario a lo ofrecido por otros planes más pequeños.  Este programa debía ser diseñado para ser regulado por los miembros académicos basado en un principio interdepartamental. Ningún departamento debía primar por sobre los demás\footnote{\url{http://med.stanford.edu/neurogradprogram/about.html}}}.
Al igual que el programa ofrecido en el MIT, se exige una rotación de 3 laboratorios el primer año, para posteriormente iniciar el trabajo de tesis\footnote{\url{http://med.stanford.edu/neurogradprogram/prospective_students/program-overview.html}}.  
Debido a la gran cantidad de profesores asociados a este programa, así como la interdepartamentalidad del mismo, es difícil establecer lineas de investigación claras\footnote{\url{http://biosciences.stanford.edu/faculty/db/\#?program=Neurosciences}}.  Sin embargo, cabe destacar que este programa cuenta con el centro de investigación para el Cerebro, la Mente y la Computación\footnote{\url{ http://web.stanford.edu/group/mbc/research.html}}, desde donde pueden desprenderse estudios que liguen la neurociencia con la ingeniería eléctrica.
Stanford también forma parte de la iniciativa CDIO.
\subsubsection{École des Neurosciences de Paris (ENP) - Francia}
La Escuela de Neurociencias de Paris (ENP) es una organización sin fines de lucro, formada por un conjunto de centros de investigación y laboratorios relacionados directa o indirectamente con el desarrollo y avance de la neurociencia en Francia. ENP fue creado el 2007 por el Ministerio Francés de Educación e Investigación, como una fundación científica.  Inicialmente contó con el apoyo de tres centros de investigación  (Inserm, CNRS and CEA) y dos universidades(Université Pierre et Marie Curie y Université Paris-Sud). 
Actualmente, el ENP cuenta con más de 100 equipos de investigación, organizados en las siguientes áreas temáticas\footnote{\url{http://www.paris-neuroscience.fr/sites/paris-neuroscience.fr/files/imce/ENP/affiche_enp_network_themes_29-02-2016.pdf}}
\begin{itemize}
\item{Neurociencia computacional y teoría neural}
\item{Neurociencia cognitiva y neuropsicología}
\item{Neurofisiología y neurociencia de sistemas}
\item{Neurofarmacología y señalización celular}
\item{Enfermedades psiquiátricas y neurológicas}
\item{Neurogenética y neurodesarrollo}
\end{itemize}
Una de sus principales funciones es la de liderar un programa interdisciplinario de doctorado, de 4 años de duración.  En este programa, completamente financiado por ENP, está abierto para postulantes de todo el mundo. Sigue el mismo patrón estadounidense, es decir, duración de 4 años, en donde el primer año se basa en una rotación por tres laboratorios. 
Además del programa de doctorado, ENP tiene un programa de pasantías de investigación para tesistas de pregrado chilenos, el cual tiene como objetivo el crear lazos científicos entre Chile y Francia, además de mostrar a los pasantes las posibilidades de continuar sus estudios en París\footnote{\url{http://www.paris-neuroscience.fr/en/chilean-interns}}.
\section{Panorama Nacional}
En esta sección se abordará la situación nacional en la enseñanza de neurociencia, tanto a nivel de pregrado como de posgrado.  Si bien en Chile no existen programas de pregrado en neurociencia, se intentará mostrar las áreas que más se asemejen.  En particular se analizará la carrera de Ingeniería Civil Biomédica en el área de pregrado, para posteriormente analizar los programas de postgrado impartidos en Chile. 
\subsection{Ingeniería Civil Biomédica}
La carrera de Ingeniería Civil Biomédica se imparte en Chile desde aproximadamente 10 años.  Actualmente es impartida como plan de pregrado en las universidades de Valparaíso y Concepción y como ''major'' en Ingeniería en la Pontificia Universidad Católica(PUC).  Este último programa podría asemejarse un poco al tipo de malla curricular planteado en el MIT.
El ingeniero biomédico es un profesional que integra en su quehacer conocimientos de las ciencias, la ingeniería y la salud, en busca de proveer soluciones que impacten positivamente la salud de las personas. Posee una sólida base en ciencias básicas y ciencias biomédicas\footnote{Las ciencias biomédicas, en particular, son aquellas disciplinas derivadas de la biología y que permiten su aplicabilidad en el área médica y clínica.}, siendo un profesional altamente capacitado para desempeñarse en las áreas de bioinstrumentación, informática médica e ingeniería clínica, como sus líneas de formación más destacadas\footnote{\url{http://www.uv.cl/carreras/?c=19076}}.

Quizá esta podría ser la carrera que más acerque la neurociencia con la ingeniería, sin embargo, analizando las mallas curriculares de las universidades que la imparten es posible concluir que no existe la posibilidad de especialización en neurociencia, neuroingeniería o neurotecnología.
\subsection{Programas de Doctorado en Neurociencias}
En Chile existen cuatro programas de doctorado en neurociencias, impartidos por la Pontificia Universidad Católica (PUC), la Universidad de Chile (UCh),  la Universidad de Valparaíso(UV) y la Universidad de Santiago(USACH).
El programa de la UC se define como "Programa Interdisciplinario en Neurociencia", e incorpora una linea de especialización en interfaces cerebro computador\footnote{\url{http://neurouc.cl/doctorado-en-neurociencia/descripcion/}}. Sin embargo, no existe ningún profesor dentro del programa, asociado a alguna facultad de Ingeniería\footnote{\url{http://neurouc.cl/miembros-del-centro/}}.
La UCh imparte su programa de Doctorado en Ciencias, con mención en Biología Molecular, Celular y Neurociencias.  Este programa no toma en cuenta herramientas o técnicas provenientes de la Ingeniería.
La UV, por su parte, tiene quizá el programa de neurociencias con mayor conexión con ingeniería.  Entre sus lineas de investigación se encuentran las áreas de Codificación Neuronal, Modelamiento matemático, simulación molecular y biología computacional\footnote{\url{http://www.dnuv.cl}}. Este programa además cuenta con colaboración directa del Centro Interdisciplinario de Neurociencias de Valparaíso\footnote{\url{http://www.cinv.cl}}.
Finalmente, el programa de la USACH presenta sólo un curso de caracter matemático (Métodos computacionales en neurociencia)\footnote{\url{http://www.quimicaybiologia.usach.cl/sites/quimicaybiologia/files/informacion_postulantes_doctorado_neurociencia_usach.pdf}}.  Sin embargo, cuenta entre sus lineas de investigación con modelamiento de sistemas fisiológicos\footnote{\url{http://www.informatica.usach.cl/academico/chacon-pacheco-max/}}.
\section{Descripción del problema}
Como se observa, a nivel mundial existe una alta sinergia entre la biología y la ingeniería.  En particular, el área de la neurociencia se ha visto beneficiado por las técnicas y modelamiento desarrollados en ingeniería eléctrica.   Esto ha permitido generar grandes proyectos de investigación multidisciplinarios, que han creado  y desarrollado tecnologías que permiten mejorar la salud y calidad de vida de las personas.

Por desgracia, en Chile falta crear ese nexo.  Los programas de pregrado en ingeniería no involucran una interacción directa con neurociencia y los programas de postgrado son escasamente interdisciplinario, generando que las colaboraciones entre neurocientíficos e ingenieros sean mínimas. Es necesario entonces elaborar y establecer puentes entre las diferentes disciplinas, para generar así nuevos conocimientos y avances en las tecnologías existentes.
El problema consiste en acercar la neurociencia al pregrado  y facilitar su enseñanza en ingeniería, de forma tal que los estudiantes, según su interés y motivación, puedan tener contacto con esta disciplina y puedan forjar nuevas oportunidades laborales y de investigación.

\section{Conclusiones}
En este capítulo se introdujo la Neurociencia como disciplina científica, enunciando algunos de los descubrimientos reaizados por ella, a lo largo de la historia. Se mostró también la interacción entre Ingeniería Eléctrica y Neurociencia, tanto en las técnicas y herramientas aplicadas en la investigación, como también en los proyectos educativos, tanto a nivel de pregrado como de posgrado en el extranjero.  Se hace latente que la interacción entre ingeniería eléctrica y neurociencia es un elemento clave para el desarrollo de ambas disciplinas y que la mejor forma de lograr dicha interacción es el de exponer a los estudiantes de pregrado de ingeniería frente a posibilidades de aprendizaje y exploración de proyectos que liguen estas dos disciplinas.
















