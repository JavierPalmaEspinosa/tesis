\chapter{Metodologías de enseñanza en ciencia e ingeniería}
\label{cap:metodos}
\section{Introducción}
Este capítulo pretende mencionar ciertas metodologías empleadas en la enseñanza universitaria de pregrado, en ciencia e ingeniería. Se revisan los conceptos y habilidades básicas que un científico y un ingeniero deben desarrollar durante su formación de pregrado, para luego mencionar ciertas técnicas que desarrollan dichas habilidades. A modo de conclusión se contrastan dichas habilidades y se intenta buscar un marco que permita desarrollar las habilidades requeridas en ambas carreras.
\section{Enseñanza en Ciencia}
La enseñanza en ciencia emplea estrategias de aprendizaje activo, que permiten involucrar a los estudiantes en el proceso.  La participación activa en las salas de clases y los laboratorios basados en descubrimientos desarrollan los hábitos mentales que son utilizados en las ciencias.  Sin embargo, la mayor parte de los cursos suele basarse en cátedras de tipo ''transmisión de información'' y ejercicios de laboratorio tipo ''recetas de cocina'' que no son extremadamente efectivas en generar el entendimiento conceptual de los fenómenos en estudio o de comprender el razonamiento científico.  Adicionalmente, existe variada evidencia que al complementar o reemplazar las estrategias de aprendizaje e involucrar a los estudiantes en el descubrimiento y el proceso científico mejora el aprendizaje y la retención de conocimiento\cite{handelsman2004scientific}.  

Adicionalmente, modelar fenómenos, derivar hipótesis y diseñar experimentos son actividades importantes en el trabajo de miles de científicos, sin embargo los libros de textos sólo presentan el final, ya sea una hipótesis probada, un modelo final y/o experimentos clásicos. Mucho del trabajo de la ciencia involucra pruebas experimentales que funcionan para modelos particulares, refutar hipótesis o incluso revelar vacíos en el entendimiento de un fenómeno, que requiere reconsiderar la situación en estudio.
Por ello, Hoskins plantea un cambio en el paradigma de enseñanza en la ciencia.  Su propuesta es emplear el método C.R.E.A.T.E\footnote{ Consider, Read, Elucidate hypotheses, Analyze and interpret data, and Think of the next Experiment.  Crear, Leer, Elucidad una hipótesis, Analizar e interpretar datos, y Pensar en el siguiente Experimento.}, el cual se basa en utilizar los textos como herramienta introductoria para el trabajo en los laboratorios científicos. Los estudiantes que emplean este método utilizan una combinación de herramientas pedagógicas para  facilitar la lectura dentro del conjunto de publicaciones científicas producidas secuencialmente por el mismo laboratorio.  De esta forma pueden examinar cómo un proyecto de investigación evoluciona a lo largo de los años.  
Este método se centra en analizar, durante muchas clases, un conjunto de publicaciones científicas\footnote{también llamados paper}, en donde cada figura o tabla es analizada en profundidad\cite{hoskins2008using}.

En resumen, Hoskins utiliza los siguientes puntos centrales en su aproximación al curso:
\begin{enumerate}
\item{ {\bf El uso de datos experimentales para desarrollar modelos explicatorios y el diseño de nuevos experimentos basados en el modelo.} La modelización es una práctica común en la investigación científica, pero pocas veces se muestra en las salas de clase.}
\item{{\bf ¿Cómo lidian los científicos con datos que no se ajustan a un modelo?¿Estos modelos están ''esculpidos en piedra'' o son meras guías para futuros análisis?} Muchos estudiantes creen que ''si algo (incluso un modelo hipotético) es publicado, entonces debe ser verdadero''.  Este malentendido hace muy difícil de darse cuenta que en la realidad, la ciencia está siempre cambiando.  Este concepto es algo que no puede ser entendido en un libro de texto, en donde todo pareciera ser verdadero y permanente.}
\item{{\bf Paradigmas y su influencia.} Incluso los investigadores más imparciales pueden verse fuertemente afectados por paradigmas existentes.}
\item{{\bf La investigación científica es una actividad ''transparente'' que involucra interacciones entre grupos, incluso aquellos que no colaboran directamente}.  La explicación e ilustración de cómo la ciencia se ''autocorrige'', debido principalmente a que los datos son compartidos y están disponibles para la reintrepretación desde otros laboratorios.}
\end{enumerate}

Con lo anterior, se hace presente entonces la necesidad de modificar la cátedra, desde una clase meramente expositiva, a una clase muchísimo más participativa, que permita la colaboración entre estudiantes y profesores y que, por sobre todo, permita al alumno el acercarse a la ciencia directamente, mediante la lectura crítica de publicaciones científicas, que le permita analizar el resultado de los experimentos y plantear posibles hipótesis alternativas o experimentos adicionales.

\section{Enseñanza en Ingeniería}
¿Qué es la Ingeniería? Fred Maillardet señala que la ingeniería es \textit{un taburete de tres patas, basado en las matemáticas, la ciencia y la técnica}.  Esta técnica es la que diferencia al ingeniero del científico, pues el ingeniero concibe, diseña, ejecuta.  Es importante entonces entender que la ingeniería es más que el entendimiento de la ciencia; es esencialmente un área vocacional que se basa en el profundo entendimiento de los principios científicos, en conjunto con una apropiada facilidad el lenguaje de la comunicación y modelamiento matemático\cite{maillardet2004outcome}

Adicionalmente, Maillardet define las habilidades básicas que cualquier ingeniero debiese desarrollar: Comunicación, Manejo de herramientas de las tecnologías de la información, aplicar matemáticas, Trabajo en equipo, Resolución de problemas, habilidad de autoperfeccionarse\cite{maillardet2004outcome}.
En particular, el autor remarca ciertas habilidades específicas que un ingeniero debiese tener.
\begin{itemize}
\item{Habilidad para transformar sistemas existentes en modelos conceptuales.}
\item{Habilidad para transformar modelos conceptuales en modelos determinados.}
\item{Habilidad para utilizar modelos determinados y obtener especificaciones en términos de parámetros.}
\item{Habilidad para seleccionar las especificaciones óptimas y crear un modelo físico.}
\item{Habilidad para aplicar los resultados de los modelos físicos a la creación de sistemas artificiales reales.}
\item{Habilidad para revisar y analizar el desempeño de los sistemas artificiales.} 
\end{itemize}

\subsection{Iniciativa CDIO}
Para lograr desarrollar las habilidades mencionadas, se ha discutido en extenso cómo enseñar ingeniería a los estudiantes\cite{baillie2004effective,kalman2008successful,wankat2015teaching}.  Una de las metodologías que han utilizado algunas de las universidades más importantes del mundo es la Iniciativa CDIO\footnote{\url{http://www.cdio.org}}.
La visión de esta iniciativa es la de entregarle a los estudiantes una educación que remarque los conceptos claves de la ingeniería, desde un punto de vista técnico. De esta forma, la enseñanza debe estar contextualizada en un modelo que permita Concebir-Diseñar-Implementar-Operar sistemas del mundo real, procesos y productos\cite{crawley2001cdio}.
El objetivo general de esta iniciativa es el de resumir formalmente el conjunto de conocimientos, habilidades y actitudes que los alumnos, la industria y la academia desea en las nuevas generaciones de ingenieros.
En particular, se plantean tres metas principales para educar a los estudiantes, quienes debiesen ser capaces de Dominar y adquirir un conocimiento profundo de los fundamentos técnicos requeridos dentro del modelo CDIO; Liderar la creación y operación de nuevos productos y sistemas; Entender la importancia e impacto estratégico de la investigación y el desarrollo tecnológico de la sociedad\footnote{\url{http://www.cdio.org/implementing-cdio/standards/12-cdio-standard}}.


Para llevar a cabo las metas propuestas, la organización de la iniciativa CDIO puede ser entendida como una adaptación del marco de trabajo de la UNESCO para el contexto de la educación en ingeniería\cite{in1996learning}.  Así, en un primer nivel, el programa de estudio CDIO se divide en cuatro categorías\cite{crawley2001cdio}
\begin{enumerate}
\item{{\bf Conocimiento técnico y razonamiento.}  Esta sección del programa de estudio define los conocimientos matemáticos, científicos y técnicos que un ingeniero debiese haber desarrollado luego de graduarse}
\item{{\bf Atributos y habilidades personales y profesionales.} Esta sección tiene relación con las habilidades individuales, la ética profesional y la resolución de problemas, capacidad de pensar creativa, crítica y sistemáticamente.}
\item{{\bf Habilidades interpersonales: Trabajo en equipo y comunicación.} La sección 3 del programa de estudios CDIO lista las habilidades que son necesarias para que el ingeniero pueda trabajar en equipo y comunicarse efectivamente.}
\item{{\bf Concebir, Diseñar, Implementar y Operar sistemas en un contexto empresarial, social y ambiental.} Finalmente, la sección 4 del programa de estudios CDIO es sobre lo que los ingeniero hacen, es decir, concebir, diseñar, implementar y operar productos, procesos y sistemas, en un contexto industrial, social y ambiental.}
\end{enumerate}

Con lo anterior, es posible entender que esta iniciativa concibe al ingeniero como un ser humano íntegro y desarrollado (categoría 2), el cuál está involucrado en un proceso(categoría 4) que se encuentra inmerso en una organización(categoría 3) que pretende construir productos(categoria 1)\cite{crawley2001cdio}. 

Como se mencionó, este programa de estudios identifica implícitamente un conjunto de habilidades requeridas por todos los ingenieros, así como también un conjunto más especifico de habilidades, según los diversos caminos profesionales que puedan continuar los egresados.
Esta iniciativa identifica a lo menos cinco caminos profesionales diferentes que los ingenieros suelen seguir, según sus talentos e intereses individuales\cite{crawley2001cdio}. 
Estos caminos son definidos mediante ciertas habilidades y destrezas claves
\begin{enumerate}
\item{El Investigador: experimentación, investigación y descubrimiento de conocimiento}
\item{El Ingeniero/Diseñador de sistemas: Concebir, Ingeniería de sistemas y administración}
\item{El Diseñador/Desarrollador de productos: Diseñar e implementar}
\item{El Ingeniero/Operador de soporte de producto: Operar}
\item{El Ingeniero Emprendedor: Contexto de negocios y empresas}
\end{enumerate}

Se ve entonces que uno de los posibles caminos que un ingeniero puede seguir consiste en la investigación.  Sin embargo, empleando sólo este plan de estudios no es posible entregar las habilidades requeridas para una disciplina exclusivamente científica.
\section{Conclusiones}
Después de analizar tanto las formas de enseñanza como las habilidades requeridas tanto para una formación científica como para una formación ingenieril, es posible resumir todo en una frase, en un desafío que sirva de guía para el desarrollo de este curso.
El desafío consiste entonces en mezclar tanto las técnicas y propuestas de enseñanza en ciencias, entregando las habilidades clave que debe tener un investigador, sin perder de vista que este curso estará dirigido a ingenieros.  
Es necesario desarrollar un programa que permita {\bf utilizar y potenciar las habilidades} que los estudiantes traen de cursos previos y {\bf redirigirlas hacia una formación científica.}  De esta forma, se cumplirán los objetivos planteados por ambos paradigmas educativos, es decir, \textit{generar en el estudiante habilidad de la lectura crítica y análisis de experimentos para que, utilizando sus habilidades de ingeniero, sea capaz de concebir, diseñar, implementar y operar los datos, hipótesis, experimentos y publicaciones propias de la ciencia.}


