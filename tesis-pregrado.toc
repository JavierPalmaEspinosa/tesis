\select@language {spanish}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {spanish}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\'Indice de Figuras}{viii}{chapter*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\unhbox \voidb@x \bgroup \let \unhbox \voidb@x \setbox \@tempboxa \hbox {I\global \mathchardef \accent@spacefactor \spacefactor }\accent 19 I\egroup \spacefactor \accent@spacefactor \futurelet \@let@token ndice de C\unhbox \voidb@x \bgroup \let \unhbox \voidb@x \setbox \@tempboxa \hbox {o\global \mathchardef \accent@spacefactor \spacefactor }\accent 19 o\egroup \spacefactor \accent@spacefactor \futurelet \@let@token digos}{ix}{chapter*.3}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n y Objetivos}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Motivaci\IeC {\'o}n}{1}{section.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.1}Neurociencia + Ingenier\IeC {\'\i }a = Neuroingenier\IeC {\'\i }a}{2}{subsection.1.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Objetivos generales y espec\IeC {\'\i }ficos}{3}{section.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.1}Objetivo General}{3}{subsection.1.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.2}Objetivos Espec\IeC {\'\i }ficos}{4}{subsection.1.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Alcances}{4}{section.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Descripci\IeC {\'o}n de cap\IeC {\'\i }tulos}{5}{section.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Antecedentes}{6}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Introducci\IeC {\'o}n}{6}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Hitos m\IeC {\'a}s importantes de la Neurociencia}{6}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Interacci\IeC {\'o}n entre Neurociencia e Ingenier\IeC {\'\i }a El\IeC {\'e}ctrica}{8}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}La Neurona como un circuito el\IeC {\'e}ctrico}{8}{subsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}El Ax\IeC {\'o}n como linea de transmisi\IeC {\'o}n}{10}{subsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}La neurona como codificador analogo digital}{11}{subsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Los circuitos neuronales como controladores \IeC {\'o}ptimos}{11}{subsection.2.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Panorama mundial}{13}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Programas gubernamentales e intergubernamentales}{13}{subsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.1}Human Connectome Project (HCP) - Estados Unidos}{13}{subsubsection.2.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.2}BRAIN Initiative - Estados Unidos}{14}{subsubsection.2.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.3}Blue Brain Project (BBP) - Europa}{15}{subsubsection.2.4.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1.4}Human Brain Project (HBP) - Europa}{16}{subsubsection.2.4.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Programas acad\IeC {\'e}micos}{16}{subsection.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.2.1}Massachusetts Institute of Technology (MIT) - Estados Unidos}{17}{subsubsection.2.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.2.2}Stanford University - Estados Unidos}{18}{subsubsection.2.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.2.3}\IeC {\'E}cole des Neurosciences de Paris (ENP) - Francia}{19}{subsubsection.2.4.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Panorama Nacional}{20}{section.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.1}Ingenier\IeC {\'\i }a Civil Biom\IeC {\'e}dica}{20}{subsection.2.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5.2}Programas de Doctorado en Neurociencias}{21}{subsection.2.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Descripci\IeC {\'o}n del problema}{21}{section.2.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.7}Conclusiones}{22}{section.2.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Metodolog\IeC {\'\i }as de ense\IeC {\~n}anza en ciencia e ingenier\IeC {\'\i }a}{23}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Introducci\IeC {\'o}n}{23}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Ense\IeC {\~n}anza en Ciencia}{23}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Ense\IeC {\~n}anza en Ingenier\IeC {\'\i }a}{25}{section.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Iniciativa CDIO}{26}{subsection.3.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Conclusiones}{28}{section.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Dise\IeC {\~n}o del curso}{29}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Introducci\IeC {\'o}n}{29}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Primera Unidad: Aspectos generales en Neurociencia}{30}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Referencias para la Unidad}{30}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.3}Segunda Unidad: Bases el\IeC {\'e}ctricas de las neuronas (biof\IeC {\'\i }sica)}{30}{section.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3.1}Referencias para la Unidad}{32}{subsection.4.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.4}Tercera Unidad: Modelos Simplificados de Neuronas (Neurociencia computacional)}{32}{section.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4.1}Referencias para la Unidad}{34}{subsection.4.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.5}Cuarta Unidad: Codificaci\IeC {\'o}n y decodificaci\IeC {\'o}n neuronal (Neurociencia te\IeC {\'o}rica)}{34}{section.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5.1}Referencias para la Unidad}{35}{subsection.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.6}Quinta Unidad: T\IeC {\'o}picos adicionales en Neurociencia}{35}{section.4.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.7}Duraci\IeC {\'o}n, formato y evaluaci\IeC {\'o}n del curso}{36}{section.4.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7.1}Duraci\IeC {\'o}n}{36}{subsection.4.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.7.2}Formato y Evaluaci\IeC {\'o}n}{36}{subsection.4.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.8}Conclusiones}{38}{section.4.8}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Dise\IeC {\~n}o del Laboratorio}{39}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.1}Introducci\IeC {\'o}n}{39}{section.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.2}Experiencia 1: Potencial de acci\IeC {\'o}n y sinapsis}{39}{section.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.3}Experiencia 2: Introducci\IeC {\'o}n a NEURON}{40}{section.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.4}Experiencia 3: Modificaci\IeC {\'o}n del potencial de acci\IeC {\'o}n debido a la carga dendr\IeC {\'\i }tica}{41}{section.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.5}Experiencia 4: Simulaci\IeC {\'o}n y c\IeC {\'a}lculo de un modelo de canal I\IeC {\'o}nico}{41}{section.5.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.6}Experiencia 5: Registro y an\IeC {\'a}lisis de ondas cerebrales mediante interfaz electr\IeC {\'o}nica}{42}{section.5.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5.7}Conclusiones}{42}{section.5.7}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {6}Conclusiones}{43}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Ap\IeC {\'e}ndices}{50}{section*.11}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {A}Apuntes para la Unidad 2}{51}{appendix.Alph1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.1}Ecuaci\IeC {\'o}n de Nernst}{51}{section.Alph1.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.2}Ecuaci\IeC {\'o}n de Goldman-Huxley-Katz}{52}{section.Alph1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.3}Ecuaci\IeC {\'o}n de Hodking y Huxley}{53}{section.Alph1.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {A.4}Ecuaciones de canales}{56}{section.Alph1.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.4.1}Ecuaciones de Canal de Sodio.}{57}{subsection.Alph1.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {A.4.2}Ecuaciones de Canal de Potasio}{57}{subsection.Alph1.4.2}
